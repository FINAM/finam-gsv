import unittest
from datetime import datetime, timedelta

import finam as fm

import finam_gsv as gsv

if __name__ == "__main__":
    data_start = datetime(2020, 1, 20)
    start = datetime(2020, 3, 1)
    request = {
        "domain": "g",
        "class": "rd",
        "expver": "hz9n",
        "type": "fc",
        "stream": "lwda",
        "anoffset": "9",
        "levtype": "sfc",
        "param": ["2t", "100u"],
        "grid": "1.0/1.0",
        "method": "nn",
    }

    g = gsv.GSV(
        request=request, data_start=data_start, start=start, step=timedelta(hours=6)
    )

    cons = fm.modules.DebugPushConsumer(
        inputs={
            "2t": fm.Info(time=None, grid=None, units="K"),
            "100u": fm.Info(time=None, grid=None, units="m s-1"),
        },
        callbacks={
            "2t": lambda n, d, t: print("RECEIVED", n, d.shape, t),
            "100u": lambda n, d, t: print("RECEIVED", n, d.shape, t),
        },
    )

    comp = fm.Composition([g, cons])
    comp.initialize()

    g.outputs["2t"] >> cons.inputs["2t"]
    g.outputs["100u"] >> cons.inputs["100u"]

    comp.run(start_time=start, end_time=start + timedelta(days=4))
