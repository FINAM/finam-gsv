import unittest
from datetime import datetime, timedelta

import finam as fm

import finam_gsv as gsv

if __name__ == "__main__":
    data_start = datetime(2020, 1, 20)
    start = datetime(2020, 3, 1)
    request = {
        "domain": "g",
        "class": "rd",
        "expver": "hz9n",
        "type": "fc",
        "stream": "lwda",
        "anoffset": "9",
        "levtype": "pl",
        "param": ["w"],
        "levelist": [500, 850],
        "grid": "1.0/1.0",
        "method": "nn",
    }

    g = gsv.GSV(
        request=request, data_start=data_start, start=start, step=timedelta(hours=6)
    )

    cons = fm.modules.DebugPushConsumer(
        inputs={
            "w_500": fm.Info(time=None, grid=None, units="Pa s-1"),
            "w_850": fm.Info(time=None, grid=None, units="Pa s-1"),
        },
        callbacks={
            "w_500": lambda n, d, t: print("RECEIVED", n, d.shape, t),
            "w_850": lambda n, d, t: print("RECEIVED", n, d.shape, t),
        },
    )

    comp = fm.Composition([g, cons])
    comp.initialize()

    g.outputs["w_500"] >> cons.inputs["w_500"]
    g.outputs["w_850"] >> cons.inputs["w_850"]

    comp.run(start_time=start, end_time=start + timedelta(days=4))
