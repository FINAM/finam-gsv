"""
FINAM component for accessing the GSV data from ``FDB``.
"""
import copy
from datetime import datetime, timedelta

import finam as fm
from gsv import GSVRetriever


class GSV(fm.TimeComponent):
    """
    FINAM component for accessing the GSV data from `FDB`.

    Parameters
    ----------

    request : dict
        Request dictionary for the GSV interface, without ``date``, ``time`` and ``step`` keys.
    data_start: datetime
        Start datetime of the data/experiment, as would be required in GSV request ``date`` and ``time``.
    start : datetime
        Start datetime for the component. Must not be before ``data_start``.
    step : timedelta
        Time step for the component.
    """

    def __init__(
        self, request: dict, data_start: datetime, start: datetime, step: timedelta
    ):
        super().__init__()
        self.time = start
        self._data_start = data_start
        self._step = step
        self._request = request

        day, time = self.to_gsv_date(self._data_start)
        self._request["date"] = day
        self._request["time"] = time

        self.gsv = GSVRetriever()
        self._initialized = False

        self.status = fm.ComponentStatus.CREATED

    @property
    def next_time(self):
        return self.time + self._step

    def _initialize(self):
        for name in self._request["param"]:
            if "levelist" in self._request:
                for level in self._request["levelist"]:
                    self.outputs.add(name=name + "_" + str(int(level)), info=None)
            else:
                self.outputs.add(name=name, info=None)

        self.create_connector()

    def _connect(self, start_time):
        push_infos = {}
        push_data = {}
        if not self._initialized:
            data = self.get_data(self._time)

            dims = tuple(data.dims)
            if dims != ("time", "lat", "lon") and dims != (
                "time",
                "level",
                "lat",
                "lon",
            ):
                raise fm.FinamDataError(f"unexpected grid dimensions: {dims}")

            nj = data.dims["lat"]
            ni = data.dims["lon"]

            lat = data.coords["lat"].data
            lon = data.coords["lon"].data

            dy = (lat[-1] - lat[0]) / (nj - 1)
            dx = (lon[-1] - lon[0]) / (ni - 1)

            grid = fm.UniformGrid(
                dims=(ni, nj),
                spacing=(abs(dx), abs(dy)),
                origin=(lon[0], lat[-1]),
                axes_reversed=True,
                axes_increase=[dx > 0, dy > 0],
                axes_names=["lat", "lon"],
                data_location=fm.Location.POINTS,
            )

            for name in self._request["param"]:
                var = data[name]
                units = var.attrs["GRIB_units"]

                if "levelist" in self._request:
                    for i, level in enumerate(self._request["levelist"]):
                        nm = name + "_" + str(int(level))
                        push_infos[nm] = fm.Info(
                            time=self._time, grid=copy.copy(grid), units=units
                        )
                        push_data[nm] = var.data[0, i, :, :]
                else:
                    push_infos[name] = fm.Info(
                        time=self._time, grid=copy.copy(grid), units=units
                    )
                    push_data[name] = var.data[0, :, :]

            self._initialized = True

        self.try_connect(
            start_time=start_time,
            push_infos=push_infos,
            push_data=push_data,
        )

    def _validate(self):
        pass

    def _update(self):
        self._time += self._step
        data = self.get_data(self._time)

        for name in self._request["param"]:
            var = data[name]

            if "levelist" in self._request:
                for i, level in enumerate(self._request["levelist"]):
                    nm = name + "_" + str(int(level))
                    self.outputs[nm].push_data(var.data[0, i, :, :], self.time)
            else:
                self.outputs[name].push_data(var.data[0, :, :], self.time)

    def _finalize(self):
        pass

    def get_data(self, time: datetime):
        step = int((time - self._data_start).total_seconds()) // 3600

        request = dict(self._request)
        request["step"] = str(step)
        return self.gsv.request_data(request)

    def to_gsv_date(self, time: datetime):
        return time.strftime("%Y%m%d"), time.strftime("%H%M")
