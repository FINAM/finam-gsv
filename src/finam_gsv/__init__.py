"""
FINAM component for accessing the GSV data from `FDB`.
"""
from .component import GSV

try:
    from ._version import __version__
except ModuleNotFoundError:  # pragma: no cover
    # package is not installed
    __version__ = "0.0.0.dev0"

__all__ = [
    "GSV",
]
