import unittest

from gsv import GSVRetriever


class TestGsv(unittest.TestCase):
    def test_gsv_simple(self):
        gsv = GSVRetriever()

        request = {
            "domain": "g",
            "class": "rd",
            "expver": "hz9n",
            "type": "fc",
            "stream": "lwda",
            "anoffset": "9",
            "date": "20200120",
            "time": "0000",
            "levtype": "sfc",
            "param": ["2t"],
            "step": "0",
            "grid": "1.0/1.0",
            "method": "nn",
        }
        data = gsv.request_data(request)

        self.assertEqual(data["2t"].shape, (1, 180, 360))
        self.assertEqual(data.coords["time"].shape, (1,))
        self.assertEqual(data.coords["lat"].shape, (180,))
        self.assertEqual(data.coords["lon"].shape, (360,))

        self.assertEqual(data["2t"].attrs["GRIB_units"], "K")

    def test_gsv(self):
        gsv = GSVRetriever()

        request = {
            "domain": "g",
            "class": "rd",
            "expver": "hz9n",
            "type": "fc",
            "stream": "lwda",
            "anoffset": "9",
            "date": "20200120",
            "time": "0000",
            "levtype": "sfc",
            "param": ["2t"],
            "step": "0/to/18/by/6",
            "grid": "1.0/1.0",
            "method": "nn",
        }
        data = gsv.request_data(request)

        self.assertEqual(data["2t"].shape, (4, 180, 360))
        self.assertEqual(data.coords["time"].shape, (4,))
        self.assertEqual(data.coords["lat"].shape, (180,))
        self.assertEqual(data.coords["lon"].shape, (360,))

        self.assertEqual(data["2t"].attrs["GRIB_units"], "K")

    def test_gsv_area(self):
        gsv = GSVRetriever()

        request = {
            "domain": "g",
            "class": "rd",
            "expver": "hz9n",
            "type": "fc",
            "stream": "lwda",
            "anoffset": "9",
            "date": "20200120",
            "time": "0000",
            "levtype": "sfc",
            "param": ["2t"],
            "step": "0/to/18/by/6",
            "grid": "1.0/1.0",
            "method": "nn",
            "area": "20/0/-10/120",
        }
        data = gsv.request_data(request)

        self.assertEqual(data["2t"].shape, (4, 30, 120))
        self.assertEqual(data.coords["time"].shape, (4,))
        self.assertEqual(data.coords["lat"].shape, (30,))
        self.assertEqual(data.coords["lon"].shape, (120,))

        self.assertEqual(data["2t"].attrs["GRIB_units"], "K")

    def test_gsv_levels(self):
        gsv = GSVRetriever()

        request = {
            "domain": "g",
            "class": "rd",
            "expver": "hz9n",
            "type": "fc",
            "stream": "lwda",
            "anoffset": "9",
            "date": "20200120",
            "time": ["0000"],
            "levtype": "pl",
            "param": ["w"],
            "levelist": [500, 850],
            "step": "0/to/18/by/6",
            "grid": "1.0/1.0",
            "method": "nn",
        }
        data = gsv.request_data(request)

        self.assertEqual(data["w"].shape, (4, 2, 180, 360))
        self.assertEqual(data.coords["time"].shape, (4,))
        self.assertEqual(data.coords["level"].shape, (2,))
        self.assertEqual(data.coords["lat"].shape, (180,))
        self.assertEqual(data.coords["lon"].shape, (360,))

        self.assertEqual(data["w"].attrs["GRIB_units"], "Pa s**-1")
