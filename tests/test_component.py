import unittest
from datetime import datetime, timedelta

import finam as fm

import finam_gsv as gsv


class TestComponent(unittest.TestCase):
    def test_component(self):
        start = datetime(2020, 1, 20)
        request = {
            "domain": "g",
            "class": "rd",
            "expver": "hz9n",
            "type": "fc",
            "stream": "lwda",
            "anoffset": "9",
            "levtype": "sfc",
            "param": ["2t", "100u"],
            "grid": "1.0/1.0",
            "method": "nn",
        }

        g = gsv.GSV(
            request=request,
            data_start=start,
            start=start + timedelta(days=5),
            step=timedelta(hours=6),
        )

        counter = 0

        def inc(n, d, t):
            nonlocal counter
            counter += 1

        cons = fm.modules.DebugPushConsumer(
            inputs={
                "2t": fm.Info(time=None, grid=None, units="K"),
                "100u": fm.Info(time=None, grid=None, units="m s-1"),
            },
            callbacks={
                "2t": inc,
            },
        )

        comp = fm.Composition([g, cons])
        comp.initialize()

        outputs = list(g.outputs.keys())
        self.assertEqual(len(outputs), 2)
        self.assertIn("2t", g.outputs)
        self.assertIn("100u", g.outputs)

        g.outputs["2t"] >> cons.inputs["2t"]
        g.outputs["100u"] >> cons.inputs["100u"]

        comp.connect()

        comp.run(start_time=start, end_time=start + timedelta(days=7))

        self.assertEqual(counter, 9)
        self.assertEqual(cons.inputs["2t"].info.grid.dims, (360, 180))

    def test_component_area(self):
        start = datetime(2020, 1, 20)
        request = {
            "domain": "g",
            "class": "rd",
            "expver": "hz9n",
            "type": "fc",
            "stream": "lwda",
            "anoffset": "9",
            "levtype": "sfc",
            "param": ["2t", "100u"],
            "grid": "1.0/1.0",
            "method": "nn",
            "area": "20/0/-10/120",
        }

        g = gsv.GSV(
            request=request,
            data_start=start,
            start=start + timedelta(days=5),
            step=timedelta(hours=6),
        )

        counter = 0

        def inc(n, d, t):
            nonlocal counter
            counter += 1

        cons = fm.modules.DebugPushConsumer(
            inputs={
                "2t": fm.Info(time=None, grid=None, units="K"),
                "100u": fm.Info(time=None, grid=None, units="m s-1"),
            },
            callbacks={
                "2t": inc,
            },
        )

        comp = fm.Composition([g, cons])
        comp.initialize()

        outputs = list(g.outputs.keys())
        self.assertEqual(len(outputs), 2)
        self.assertIn("2t", g.outputs)
        self.assertIn("100u", g.outputs)

        g.outputs["2t"] >> cons.inputs["2t"]
        g.outputs["100u"] >> cons.inputs["100u"]

        comp.connect()

        comp.run(start_time=start, end_time=start + timedelta(days=7))

        self.assertEqual(counter, 9)
        self.assertEqual(cons.inputs["2t"].info.grid.dims, (120, 30))

    def test_component_levels(self):
        start = datetime(2020, 1, 20)
        request = {
            "domain": "g",
            "class": "rd",
            "expver": "hz9n",
            "type": "fc",
            "stream": "lwda",
            "anoffset": "9",
            "levtype": "pl",
            "param": ["w"],
            "levelist": [500, 850],
            "grid": "1.0/1.0",
            "method": "nn",
        }

        g = gsv.GSV(
            request=request,
            data_start=start,
            start=start + timedelta(days=5),
            step=timedelta(hours=6),
        )

        counter = 0

        def inc(n, d, t):
            nonlocal counter
            counter += 1

        cons = fm.modules.DebugPushConsumer(
            inputs={
                "w_500": fm.Info(time=None, grid=None, units="Pa s**-1"),
                "w_850": fm.Info(time=None, grid=None, units="Pa s**-1"),
            },
            callbacks={
                "w_500": inc,
            },
        )

        comp = fm.Composition([g, cons])
        comp.initialize()

        outputs = list(g.outputs.keys())
        self.assertEqual(len(outputs), 2)
        self.assertIn("w_500", g.outputs)
        self.assertIn("w_850", g.outputs)

        g.outputs["w_500"] >> cons.inputs["w_500"]
        g.outputs["w_850"] >> cons.inputs["w_850"]

        comp.connect()

        comp.run(start_time=start, end_time=start + timedelta(days=7))

        self.assertEqual(counter, 9)
